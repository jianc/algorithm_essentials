#include "boost_flat_container.hh"
#include <iostream>
#include <memory>
#include <string>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <vector>
#include "k.hh"

int main()
{
    const int k = 11;
    std::string s1 = "11A";
    std::string s2 = "22A";
    std::cout<< k_plus(s1, s2, k)<<std::endl;
    std::cout<< k_minus(s1, s2, k)<<std::endl;
    std::cout<< k_multi(s1, s2, k)<<std::endl;

    return 0;
}
