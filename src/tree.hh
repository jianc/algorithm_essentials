#pragma once
#include <iostream>
#include <array>
#include <unordered_set>
template <typename T> struct Node
{
    Node(const T &value, T *left, T *right)
        : value(value), left(left), right(right)
    {
    }
    T value;
    Node *left;
    Node *right;
};
template <typename T> class BinaryTree
{
    using node = Node<T>;
  public:
    BinaryTree() : root(nullptr) {}
    ~BinaryTree() {}
    void insert(const T &key)
    {
        if (nullptr != root)
            insert(key, root);
        else
            root = new node(key, nullptr, nullptr);
    }
    node *search(const T &key)
    {
        return nullptr != root ? search(key, root) : nullptr;
    }

  private:
    node *root;
    void insert(const T &key, node *leaf)
    {
        if(nullptr == leaf)
            leaf = new node(key, nullptr, nullptr);
        else if(key > leaf->value)
            insert(key, leaf->left);
        else if( key < leaf->value)
            insert(key, leaf->right);
        else
            return;
    }
    node *search(const T &key, node *leaf)
    {
        if(nullptr == leaf)
            return nullptr;
        else if(key > leaf->value)
            return search(key, leaf->left);
        else if(key < leaf->value)
            return search(key, leaf->right);
        else
            return leaf;
    }
};
std::array<bool, 100> get_lock()
{
    std::array<bool, 100> v_status = {false};
    std::unordered_set<size_t> v_ids{1, 4, 9, 16, 25, 36, 49, 64, 81};
    for(size_t i = 0; i < v_status.size(); ++i)
        v_status[i] = (v_ids.end() != v_ids.find(i));
    return v_status;
}

