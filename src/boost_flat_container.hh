
#include <boost/container/small_vector.hpp>
#include <boost/container/flat_map.hpp>                                                                           
#include <boost/container/flat_set.hpp>

template <class T, std::size_t N>
using small_vector = boost::container::small_vector<T, N>;
//线性存储，insert 时间复杂度 N * N
//std::map insert 是 N * log(N)
//所以数组比较小的用flat效率比较高
template <class... Ts> using flat_map = boost::container::flat_map<Ts...>;
template <class... Ts> using flat_set = boost::container::flat_set<Ts...>;     
