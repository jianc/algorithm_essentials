#pragma once
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

struct Compress
{
    static constexpr long C_MAX_BUFFER_SIZE = 4096 * 60;
    unsigned char compress_bin[C_MAX_BUFFER_SIZE] = {0};
    unsigned long compress_bin_size = 0;
    Compress(const unsigned char *input_buf, const unsigned long input_length)
    {
        assert(input_buf != nullptr && "input ptr is nullptr.");
        uLong compr_len = C_MAX_BUFFER_SIZE;
        Byte *compr = static_cast<Byte *>(compress_bin);
        int err = compress(compr, &compr_len, (const Bytef *)input_buf,
                           static_cast<uLong>(input_length));
        if(err != Z_OK)
            std::cout << (err == Z_OK) << " " << (err == Z_MEM_ERROR) << " "
                 << (err == Z_BUF_ERROR) << std::endl;
        compress_bin_size = static_cast<unsigned long>(compr_len);
        std::cout << "orignal size: " << input_length
                  << " , compressed size : " << compress_bin_size << endl;
    }
};

struct UnCompress
{
    static constexpr long UC_MAX_BUFFER_SIZE = 4096 * 60;

    Byte uncompr[UC_MAX_BUFFER_SIZE];
    uLong uncompr_len = UC_MAX_BUFFER_SIZE;
    unsigned char *uncompress_bin;
    unsigned long uncompress_bin_size;

    UnCompress(const unsigned char *input_buf, const unsigned long input_length)
    {
        int err = uncompress(uncompr, &uncompr_len, input_buf, input_length);
        if(err != Z_OK)
            std::cout << (err == Z_OK) << " " << (err == Z_MEM_ERROR) << " "
                 << (err == Z_BUF_ERROR) << std::endl;
        uncompress_bin = (unsigned char *)uncompr;
        uncompress_bin_size = (unsigned long)uncompr_len;
        std::cout << "compress size: " << input_length
             << "  uncompressed size : " << uncompress_bin_size << std::endl;
    }
};
