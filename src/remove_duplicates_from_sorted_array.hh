#include <iostream>
#include <iterator>
#include <vector>
class ReduceDuplicates
{
  public:
    int algorithm(std::vector<int> &input_array)
    {
        if (0 == input_array.size())
            return 0;
        int count = 0, inv_count = 0;
        for (size_t i = 1; i < input_array.size(); ++i)
            if (input_array[count] != input_array[i])
                input_array[++count] = input_array[i];
            else
                ++inv_count;
        while (inv_count-- > 0)
        {
            input_array.pop_back();
        }
        return count + 1;
    }
    void test()
    {
        std::vector<int> test_array{1, 2, 3, 3, 4, 5, 5, 6};
        std::cout << algorithm(test_array) << std::endl;
        std::copy(test_array.begin(), test_array.end(),
                  std::ostream_iterator<int>(std::cout, " "));
        std::cout << std::endl;
    }
};
class ReduceDuplicatesTwice
{
  public:
    int algorithm(std::vector<int> &input_array)
    {
        if (2 >= input_array.size())
            return input_array.size();
        int count = 1, inv_count = 0;
        for (size_t i = 2; i < input_array.size(); ++i)
            if (input_array[count - 1] != input_array[i])
                input_array[++count] = input_array[i];
            else
                ++inv_count;
        while (inv_count-- > 0)
        {
            input_array.pop_back();
        }
        return count + 1;
    }
    void test()
    {
        std::vector<int> test_array{1, 2, 3, 3, 3, 4, 5, 5, 6};
        std::cout << algorithm(test_array) << std::endl;
        std::copy(test_array.begin(), test_array.end(),
                  std::ostream_iterator<int>(std::cout, " "));
        std::cout << std::endl;
    }
};
class LongestConsecutiveSequence
{
    //by sorted -> O(nlog(n))
    void algorithm()
    {

    }
    //by hash -> O(n)
    void algorithm1()
    {

    }
    void test()
    {
        std::vector<int> test_array{100, 4, 200, 1, 3, 2};
        
        std::cout << algorithm(test_array) << std::endl;
        std::cout << std::endl;
    }
};

