//const
//避免代码重复
class TextBlock
{
  public:
    const char &operator[](std::size_t position) const
    {
        /*
         * do something repeatable.
         */
        return text[postion];
    }
    char &operator[](std::size_t position)
    {
        return const_cast<char &>(static_cast<const TextBlock &>(*this));
    }
};

//确保对象在使用前已经被初始化
class FileSystem
{
    public:
        std::size_t numDisks() const;
};
extern FileSystem tfs;

class Directory
{
    public:
        Directory(params);
};
Directory::Directory(params)
{
    std::size_t disks = tfs.numDisks();
}
Directory tempDir(params);
//无法保证tfs在Directory之前被初始化。
//换一种写法： 单例
//多线程中会有麻烦，处理方式：在单线程阶段进行初始化，避免race conditions
class FileSystem{ ... };
FileSystem &tfs()
{
    //local static 作用于在类内
    static FileSystem fs;
    return fs;
}
class Directory(params){ ... }
Directory::Directory(params)
{
    std::size_t disks = tfs().numDisks();
}

Directory & tempDir()
{
    static Directory td;
    return td;
}
//拒绝copy construction 和 copy assignment
//在连接期间报错
class HomeForSale
{
    public:
        ...
    private:
        //只声明不定义
        HomeForSale(const HomeForSale &);
        HomeForSale& operator=(const HomeForSale&);
};
//在编译期间报错
//只需要继承这个类
class Uncopyable
{
    protected:
        Uncopyable(){}
        ~Uncopyable(){}
    private:
        Uncopyable(const Uncopyable&);
        Uncopyable& operator=(const Uncopyable&);
};
//将base class 的析构函数声明为virtual，如果需要一个纯抽象class可以将析构函数声明为pure virtual
//析构的顺序为按照派生的逆序，最后析构base class
