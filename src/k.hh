#pragma once
#include <algorithm>
#include <iostream>
#include <string>
int from_K_to_10(std::string &num, const int k)
{
    int sum = 0;
    int len = static_cast<int>(num.size()) - 1;
    for (int i = len; i >= 0; i--)
    {
        auto c = num[i];
        const auto ascii = int(c);
        if (48 <= ascii && ascii <= 57)
            sum += (ascii - 48) * std::pow(k, len - i);
        else if (65 <= ascii && ascii <= 90)
            sum += (ascii - 55) * std::pow(k, len - i);
        else
            std::cout << "error.";
    }
    return sum;
}
std::string from_10_to_K(const int num, const int k)
{
    std::string k_num;
    bool negative = (num < 0);
    int n = std::abs(num) / k;
    int m = std::abs(num) % k;
    while (true)
    {
        if (0 <= m && m <= 9)
            k_num += char(m + 48);
        else if (9 < m && m <= k)
            k_num += char(m + 55);
        else
            std::cout << "error." << std::endl;
        if (n == 0)
            break;
        m = n % k;
        n = n / k;
    }
    if (negative)
        k_num.append("-");
    return std::string(k_num.crbegin(), k_num.crend());
}
std::string k_plus(std::string &s1, std::string &s2, const int k)
{
    return from_10_to_K((from_K_to_10(s1, k) + from_K_to_10(s2, k)), k);
}
std::string k_minus(std::string &s1, std::string &s2, const int k)
{
    return from_10_to_K((from_K_to_10(s1, k) - from_K_to_10(s2, k)), k);
}
std::string k_multi(std::string &s1, std::string &s2, const int k)
{
    return from_10_to_K((from_K_to_10(s1, k) * from_K_to_10(s2, k)), k);
}
