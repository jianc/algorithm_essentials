#pragma once
#include <momery>
template <class T> class RingBuffer
{
  public:
    explicit RingBuffer(size_t size)
        : buffer(std::unique_ptr<T[]>(new T[size])), max_size(size);

    void put(T item)
    {
        std::lock_guard<std::mutex> lock(m);
        buffer[head] = item;
        if(is_full)
            tail = (tail + 1) % max_size;
        head = (head+1) % max_size;
        is_full = (head == tail);
    }
    T get()
    {
        std::lock_guard<std::mutex> lock(m);
        if(empty())
            return T();
        auto val = buffer[tail];
        is_full = false;
        tail = (tail + 1) % max_size;
        return val;
    }
    void reset()
    {
        std::lock_guard<std::mutex> lock(m);
        head = tail;
        is_full = false;
    }
    bool empty() const { return (!is_full && (head == tail)); }
    bool full() const { return is_full; }
    size_t capacity() const {return max_size; }
    size_t size() const
    {
        return is_full ? max_size : (head >= tail ? head - tail
                                                 : max_size + head - tail);
    };

  private:
    std::mutex m;
    std::unique_ptr<T[]> buffer;
    size_t head = 0;
    size_t tail = 0;
    const size_t max_size;
    bool is_full = false;
};

//usage:
//RingBuffer<> ring_buffer(10);
//ring_buffer.put();
//ring_buffer.get();
